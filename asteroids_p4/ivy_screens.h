#ifndef IVY_SCREENS_H
#define IVY_SCREENS_H

#include <iostream>
#include <sstream>
#include <vector>
using namespace std;

#include "ivy_globals.h"
#include "ivy_enums.h"
#include "ivy_sprites.h"

namespace ivy{

	// to-do
	// load screen resources (e.g. sprites images) in threads
	// keep polling for resource complete load flags before changing state from SCREEN_INIT to SCREEN_TRANSITION_IN
	// make sure to show "loading" message in SCREEN_INIT in the meantime

	// stopped
	// after done, figure out a way to test sprites in game logic
	// then, focus on asteroids engine!

	// ==================================================
	// 
	// game screens
	// 
	// ==================================================

	class Screen{
	protected:
		static int ID_COUNTER;
		int screen_id;
		sf::RenderWindow *window;
		ivy::ScreenState screen_state;
		ivy::GameState next_screen;
		virtual void loadResource(){
		}
	public:
		Screen(){
			screen_id = ++ID_COUNTER;
			screen_state = ivy::SCREEN_INIT;
			next_screen = ivy::GAME_NOTHING;
			cout << "+ Screen\n";
		}
		virtual ~Screen(){
			cout << "- Screen : " << screen_id << "\n";
			// delete window;
		}
		int getScreenId(){
			return screen_id;
		}
		ivy::ScreenState getScreenState(){
			return screen_state;
		}
		ivy::GameState getNextScreen(){
			return next_screen;
		}

		void setWindow( sf::RenderWindow *w ){
			window = w;
		}

		void update( sf::Int32 time_elapsed ){
			switch (screen_state){
				case ivy::SCREEN_INIT:
					// load resources
					updateInit( time_elapsed );
					break;

				case ivy::SCREEN_TRANSITION_IN:
					// transition the curtain alpha
					updateTransitionIn( time_elapsed );
					break;

				case ivy::SCREEN_ACTIVE:
					// main logic goes here
					updateActive( time_elapsed );
					break;

				case ivy::SCREEN_INACTIVE:
					// don't do anything, but can still draw
					updateInactive( time_elapsed );
					break;

				case ivy::SCREEN_TRANSITION_OUT:
					// transition the curtain alpha
					updateTransitionOut( time_elapsed );
					break;
			}
		}
		virtual void updateInit( sf::Int32 time_elapsed ){
			cout << "===== updateInit\n";
			screen_state = ivy::SCREEN_TRANSITION_IN;
		}
		virtual void updateTransitionIn( sf::Int32 time_elapsed ){
			cout << "===== updateTransitionIn\n";
			screen_state = ivy::SCREEN_ACTIVE;
		}
		virtual void updateActive( sf::Int32 time_elapsed ){
			// cout << "===== updateActive\n";
			// if not overridden, at least listen for close
			sf::Event e;
			while ( (*window).pollEvent(e) ){
				if ( e.type == sf::Event::Closed ){
					(*window).close();
				}
			}
			// screen_state = ivy::SCREEN_INACTIVE;
		}
		virtual void updateInactive( sf::Int32 time_elapsed ){
			cout << "===== updateInactive\n";
			// if not overridden, at least listen for close
			sf::Event e;
			while ( (*window).pollEvent(e) ){
				if ( e.type == sf::Event::Closed ){
					(*window).close();
				}
			}
			screen_state = ivy::SCREEN_TRANSITION_OUT;
		}
		virtual void updateTransitionOut( sf::Int32 time_elapsed ){
			cout << "===== updateTransitionOut\n";
			screen_state = ivy::SCREEN_DEAD;
		}

		void draw(){
			switch (screen_state){
				case ivy::SCREEN_INIT:
					drawInit();
					break;

				case ivy::SCREEN_TRANSITION_IN:
					drawTransitionIn();
					break;

				case ivy::SCREEN_ACTIVE:
					drawActive();
					break;

				case ivy::SCREEN_INACTIVE:
					drawInactive();
					break;

				case ivy::SCREEN_TRANSITION_OUT:
					drawTransitionOut();
					break;
			}
		}
		virtual void drawInit(){
		}
		virtual void drawTransitionIn(){
		}
		virtual void drawActive(){
		}
		virtual void drawInactive(){
		}
		virtual void drawTransitionOut(){
		}
	};

	class Screen_Intro : public Screen{
	private:
		// reserved for logo screens
	public:
		void drawActive(){
			next_screen = ivy::GAME_MAIN_MENU;
			screen_state = ivy::SCREEN_TRANSITION_OUT;
		}
	};

	class Screen_Main_Menu : public Screen{
	private:
		static const int MENU_SIZE = 3;
		int menu_hl_index;
		int menu_hl_alpha;
		int menu_hl_alpha_dir;
		const char* _menu_items[MENU_SIZE];
		sf::Font font;
		void loadResource(){
			if ( !font.loadFromFile("assets/georgia.ttf") ){
				throw 0;
			}
		}
	public:
		Screen_Main_Menu(){
			cout << "+ Screen_Main_Menu : " << screen_id << "\n";

			loadResource();

			menu_hl_index = 0;
			menu_hl_alpha = 255;
			menu_hl_alpha_dir = -1;
			_menu_items[0] = "Play Asteroids";
			_menu_items[1] = "Reset Hi-Score";
			_menu_items[2] = "Quit";
		}
		~Screen_Main_Menu(){
			cout << "- Screen_Main_Menu : " << screen_id << "\n";
		}
		void updateActive( sf::Int32 time_elapsed ){
			// get event for polling
			sf::Event e;

			while ( (*window).pollEvent(e) ){
				// get key code
				sf::Keyboard::Key key = e.key.code;

				// check for closing window
				if (e.type == sf::Event::Closed){
					(*window).close();
				}

				// menu navigation
				if (e.type == sf::Event::KeyPressed){
					// move menu highlight
					if (key == sf::Keyboard::Up){
						menu_hl_index--;
					}
					if (key == sf::Keyboard::Down){
						menu_hl_index++;
					}

					// select menu item
					if (key == sf::Keyboard::Return){
						switch ( menu_hl_index ){
							case 0:
								// play game
								screen_state = ivy::SCREEN_TRANSITION_OUT;
								next_screen = ivy::GAME_PLAY;
								break;

							case 1:
								// reset hi score only
								ivy::HI_SCORE = 0;
								break;

							case 2:
								// quit game
								// NOTE: the reason we don't call 
								// window.close() here is because we
								// allow the screen to transition out 
								// smoothly before exiting game.
								screen_state = ivy::SCREEN_TRANSITION_OUT;
								next_screen = ivy::GAME_EXIT;
								break;
						}
					}
				}
			}

			// make sure menu highlight is within list
			if (menu_hl_index < 0){
				menu_hl_index = 0;
			}
			if (menu_hl_index > MENU_SIZE-1){
				menu_hl_index = MENU_SIZE-1;
			}

			// NOTE
			// animate by time instead of frames
			// assume 255 to 55 alpha takes 500ms
			// thus, hl_alpha_total = 200
			// and hl_speed = 500
			// thus, it takes 2.5ms to change 1 alpha
			int new_alpha = int(time_elapsed / ivy::MENU_HL_SPEED * menu_hl_alpha_dir);
			menu_hl_alpha += new_alpha;

			// update highlight glow
			if (menu_hl_alpha < 50){
				menu_hl_alpha = 50;
				menu_hl_alpha_dir *= -1;
			}
			if (menu_hl_alpha > 255){
				menu_hl_alpha = 255;
				menu_hl_alpha_dir *= -1;
			}
		}
		void drawActive(){
			for (int i = 0; i < MENU_SIZE; i++){
				// set variables
				const char* t = _menu_items[i];
				sf::Text text(t, font, 50);
				sf::Color color;

				// position the text
				float x = 50;
				float y = float(i * 60 + 50);

				// if selected, is flashing red.
				// else, white
				if (i == menu_hl_index){
					color = sf::Color( 255, 0, 0, menu_hl_alpha );
				}
				else{
					color = sf::Color::White;
				}

				// set text properties
				text.setPosition( x, y );
				text.setColor( color );

				// draw text
				(*window).draw( text );
			}
		}
	};

	class Screen_Play : public Screen{
	private:
		// resource
		sf::Texture spritesheet; // full spritesheet containing all animation images
		sf::Font font;

		// game objects
		sf::Text text_score;
		ivy::Sprite_Ship* ship;
		std::vector<ivy::Sprite*> _rocks;

		void loadResource(){
			// load font
			if ( !font.loadFromFile("assets/georgia.ttf") ){
				throw 0;
			}

			// for asteroids, we put all the sprites in one sheet
			if ( !spritesheet.loadFromFile( "assets/main.png" ) ){
				throw 0;
			}

			// add color masking - set #FF00FF color as transparent
			sf::Image image = spritesheet.copyToImage();
			image.createMaskFromColor( sf::Color(255,0,255), 0 );
			spritesheet.loadFromImage( image );
		}
	public:
		Screen_Play(){
			cout << "+ Screen_Play : " << screen_id << "\n";

			loadResource();

			// reset the objects
			ship = new Sprite_Ship();
			(*ship).setSpritesheet( &spritesheet );

			// set the ship to start from center
			float center_x = ivy::WINDOW_W / 2;
			float center_y = ivy::WINDOW_H / 2;
			(*ship).setPosition( center_x, center_y );

			text_score.setFont(font);
			text_score.setCharacterSize(16);

			// reset current score every time we play
			ivy::SCORE = 0;
		}
		~Screen_Play(){
			// to-do
			// delete ship, bullet, rock
			cout << "PLAY screen destructor : " << screen_id << "\n";
			cout << "- Screen_Play : " << screen_id << "\n";

			// delete ship and its containing bullets
			delete ship;

			// delete rocks
			std::vector<ivy::Sprite*>::iterator it;
			for (it = _rocks.begin(); it != _rocks.end(); it++){
				delete (*it);
			}
			_rocks.clear();
		}
		void updateActive( sf::Int32 time_elapsed ){
			// to-do
			// check for WSAD for movement
			// check for spacebar for shoot
			// check collision between bullet/asteroid
			// check collision between player/asteroid
			// if died, start timer to respawn after 3 seconds
			// if no more life, go to game over screen

			// ==================================================
			// 
			// update events
			// 
			// ==================================================

			// get event for polling
			sf::Event e;

			while ( (*window).pollEvent(e) ){
				// get key code
				sf::Keyboard::Key key = e.key.code;

				// check for closing window
				if (e.type == sf::Event::Closed){
					(*window).close();
				}

				// ship action

				if (e.type == sf::Event::KeyPressed){
					// exit to game over
					if (key == sf::Keyboard::Escape){
						screen_state = ivy::SCREEN_TRANSITION_OUT;
						next_screen = ivy::GAME_OVER;
					}

					// move ship
					if (key == sf::Keyboard::Up){
						(*ship).accelerate();
					}
					if (key == sf::Keyboard::Down){
						(*ship).decelerate();
					}
					if (key == sf::Keyboard::Left){
						(*ship).turnLeft();
					}
					if (key == sf::Keyboard::Right){
						(*ship).turnRight();
					}

					// shoot bullet
					if (key == sf::Keyboard::Space){
						// auto-fire until key is released
						(*ship).startShoot();
					}
				}

				if (e.type == sf::Event::KeyReleased){

					// move ship
					if (key == sf::Keyboard::Up){
						(*ship).stopAccelerate();
					}
					if (key == sf::Keyboard::Down){
						(*ship).stopDecelerate();
					}
					if (key == sf::Keyboard::Left){
						(*ship).stopTurn();
					}
					if (key == sf::Keyboard::Right){
						(*ship).stopTurn();
					}

					// shoot bullet
					if (key == sf::Keyboard::Space){
						(*ship).stopShoot();
					}
				}
		    }

			// ==================================================
			// 
			// update ship and bullets
			// 
			// ==================================================

		    // to-do:
		    // multiplayer ship should have vector of ships

			(*ship).update( time_elapsed );

			// ==================================================
			// 
			// update rocks
			// 
			// ==================================================

			// if (_rocks.size() > 0){
			// 	std::vector<Sprite*>::iterator i;
			// 	for (i = _rocks.begin(); i != _rocks.end(); ++i){
			// 		(*i)->update( window );
			// 	}
			// }

			// ==================================================
			// 
			// collision logic goes here
			// 
			// ==================================================

			// to-do

			// ==================================================
			// 
			// update score
			// 
			// ==================================================

			// test: check the text and position actually updates
			ivy::SCORE++;

			// set text properties
			std::stringstream stream;
			stream << "Score : " << ivy::SCORE << " | " << "Hi-Score : " << ivy::HI_SCORE;
			text_score.setString( stream.str() );

			// get the text position at top-right corner
			sf::Vector2u window_size = (*window).getSize();
			sf::FloatRect text_bound = text_score.getGlobalBounds();
			sf::Vector2i text_pos;

			// update position
			float text_x = window_size.x - text_bound.width - 10; // padding
			float text_y = 0;
			text_score.setPosition( text_x, text_y );

			// ==================================================
			// 
			// update explosion, respawn, game over
			// 
			// ==================================================
		}
		void drawActive(){

			// ==================================================
			// 
			// draw ship
			// 
			// ==================================================

			(*ship).draw( window );

			// ==================================================
			// 
			// draw rocks
			// 
			// ==================================================

			if (_rocks.size() > 0){
				std::vector<Sprite*>::iterator i;
				for (i = _rocks.begin(); i != _rocks.end(); ++i){
					(*i)->draw( window );
				}
			}

			// ==================================================
			// 
			// draw score text
			// 
			// ==================================================

			(*window).draw( text_score );
		}
	};

	class Screen_Over : public Screen{
	private:
		static const int MENU_SIZE = 2;
		int menu_hl_index;
		int menu_hl_alpha;
		int menu_hl_alpha_dir;
		const char* _menu_items[MENU_SIZE];
		sf::Font font;
		void loadResource(){
			if ( !font.loadFromFile("assets/georgia.ttf") ){
				throw 0;
			}
		}
	public:
		Screen_Over(){
			cout << "+ Screen_Over : " << screen_id << "\n";

			loadResource();

			menu_hl_index = 0;
			menu_hl_alpha = 255;
			menu_hl_alpha_dir = -1;
			_menu_items[0] = "Try Again";
			_menu_items[1] = "Return to Main Menu";
		}
		~Screen_Over(){
			cout << "- Screen_Over : " << screen_id << "\n";
		}
		void updateActive( sf::Int32 time_elapsed ){
			// get event for polling
			sf::Event e;

			while ( (*window).pollEvent(e) ){
				// get key code
				sf::Keyboard::Key key = e.key.code;

				// check for closing window
				if (e.type == sf::Event::Closed){
					(*window).close();
				}

				// menu navigation
				if (e.type == sf::Event::KeyPressed){
					// move menu highlight
					if (key == sf::Keyboard::Up){
						menu_hl_index--;
					}
					if (key == sf::Keyboard::Down){
						menu_hl_index++;
					}

					// select menu item
					if (key == sf::Keyboard::Return){
						switch ( menu_hl_index ){
							case 0:
								// re-play game
								screen_state = ivy::SCREEN_TRANSITION_OUT;
								next_screen = ivy::GAME_PLAY;
								break;

							case 1:
								// quit game
								// NOTE: the reason we don't call 
								// window.close() here is because we
								// allow the screen to transition out 
								// smoothly before exiting game.
								screen_state = ivy::SCREEN_TRANSITION_OUT;
								next_screen = ivy::GAME_MAIN_MENU;
								break;
						}
					}
				}
			}

			// make sure menu highlight is within list
			if (menu_hl_index < 0){
				menu_hl_index = 0;
			}
			if (menu_hl_index > MENU_SIZE-1){
				menu_hl_index = MENU_SIZE-1;
			}

			int new_alpha = int(time_elapsed / ivy::MENU_HL_SPEED * menu_hl_alpha_dir);
			menu_hl_alpha += new_alpha;

			// update highlight glow
			if (menu_hl_alpha < 50){
				menu_hl_alpha = 50;
				menu_hl_alpha_dir *= -1;
			}
			if (menu_hl_alpha > 255){
				menu_hl_alpha = 255;
				menu_hl_alpha_dir *= -1;
			}
		}
		void drawActive(){
			for (int i = 0; i < MENU_SIZE; i++){
				// set variables
				const char* t = _menu_items[i];
				sf::Text text(t, font, 50);
				sf::Color color;

				// position the text
				float x = 50;
				float y = float(i * 60 + 50);

				// if selected, is flashing red.
				// else, white
				if (i == menu_hl_index){
					color = sf::Color( 255, 0, 0, menu_hl_alpha );
				}
				else{
					color = sf::Color::White;
				}

				// set text properties
				text.setPosition( x, y );
				text.setColor( color );

				// draw text
				(*window).draw( text );
			}

			// draw score
			std::stringstream text_score;
			text_score << "Hi-score : " << ivy::HI_SCORE;
			sf::Text text(text_score.str(), font, 20);
			sf::Color color = sf::Color( 0, 255, 255 );
			text.setPosition( 50, 400 );
			text.setColor( color );
			(*window).draw(text);
		}
	};

	// ==================================================
	// 
	// screen manager
	// 
	// ==================================================

	class ScreenManager{
	private:
		std::vector<Screen*> _screens;
		sf::RenderWindow *window;

		ivy::Screen* getScreenById( int target_id = 0 ){
			std::vector<Screen*>::iterator i;
			ivy::Screen* screen;
			for (i = _screens.begin(); i != _screens.end(); ++i){
				int id = (*i)->getScreenId();
				if (id == target_id){
					screen = *(i._Ptr);
				}
			}
			return screen;
		}
		void removeScreenById( int id ){
			// the actual screen removal is a manager-only method.
			// when deleting a screen, we actually transition it out
			// before setting SCREEN_DEAD state to it, which will
			// trigger this function from update();

			// NOTE
			// vector's erase only removes the pointer, but the object still exists
			// we need to manually call delete to actually delete the object

			std::vector<Screen*>::iterator it;
			for (it = _screens.begin(); it != _screens.end(); it++){
				int this_id = (*it)->getScreenId();
				if ( this_id == id ){
					// http://www.cplusplus.com/reference/vector/vector/erase/
					delete (*it);
					_screens.erase( it );
					break;
				}
			}
		}
	public:
		void update( sf::Int32 time_elapsed ){
			// iterate from last to first, because the latest
			// screen is usually the last in the list
			std::vector<int> _to_delete;
			ivy::GameState next_screen = ivy::GAME_NOTHING;

			// if the list is empty, quit!
			if ( _screens.begin() == _screens.end() ){
				(*window).close();
				return;
			}

			std::vector<Screen*>::reverse_iterator rit;
			for (rit = _screens.rbegin(); rit != _screens.rend(); ++rit){

				// update screen
				(*rit)->update( time_elapsed );

				// get current state
				ivy::ScreenState state = (*rit)->getScreenState();

				// if screen is dead
				if ( state == ivy::SCREEN_DEAD ){
					// add id to _to_delete
					int this_id = (*rit)->getScreenId();
					_to_delete.push_back( this_id );

					// get next screen to show (if any)
					next_screen = (*rit)->getNextScreen();
				}
			}

			// iterate through list and delete screens in _to_delete
			std::vector<int>::iterator it;
			for (it = _to_delete.begin(); it != _to_delete.end(); ++it){
				removeScreenById( *it );
			}

			// if there is a next screen, add it to queue
			if ( next_screen != ivy::GAME_NOTHING ){
				addScreen( next_screen );
			}
		}
		void draw(){
			std::vector<Screen*>::reverse_iterator i;
			for (i = _screens.rbegin(); i != _screens.rend(); ++i){
				// draw screen
				(*i)->draw();
			}
		}
		void addScreen( ivy::GameState game_state ){
			cout << "addScreen : ";
			Screen* screen;
			switch ( game_state ){
				case ivy::GAME_INTRO:
					cout << "GAME_INTRO\n";
					screen = new Screen_Intro();
					break;

				case ivy::GAME_MAIN_MENU:
					cout << "GAME_MAIN_MENU\n";
					screen = new Screen_Main_Menu();
					break;

				case ivy::GAME_PLAY:
					cout << "GAME_PLAY\n";
					screen = new Screen_Play();
					break;

				case ivy::GAME_OVER:
					cout << "GAME_OVER\n";
					screen = new Screen_Over();
					break;

				case ivy::GAME_EXIT:
					(*window).close();
					return;
					break;

				case ivy::GAME_NOTHING:
				default:
					// do nothing
					return;
					break;
			}

			// NOTE
			// will throw error if screen is not init
			// so be careful!
			(*screen).setWindow( window );
			_screens.push_back( screen );
		}
		void setWindow( sf::RenderWindow *w ){
			window = w;
		}
	};

	// init static variables
	int Screen::ID_COUNTER = 0;
}

#endif