#ifndef IVY_GLOBALS_H
#define IVY_GLOBALS_H

namespace ivy{
	// game constants
	const char* GAME_TITLE = "Team Asteroids";
	const unsigned int WINDOW_W = 800;
	const unsigned int WINDOW_H = 600;
	const unsigned int FRAME_RATE_LIMIT = 60;
	const double PI = std::atan(1.0)*4.0;

	// game variables
	static int HI_SCORE = 0;
	static int SCORE = 0;
	static float MENU_HL_SPEED = 2.5; // 2.5 alpha/ms
}

#endif