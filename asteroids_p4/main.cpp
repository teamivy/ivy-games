#include <SFML\Graphics.hpp>

#include "ivy_globals.h"
#include "ivy_enums.h"
#include "ivy_screens.h"

int main(){

	// create window and set parameters
	sf::RenderWindow window( sf::VideoMode( ivy::WINDOW_W, ivy::WINDOW_H ), ivy::GAME_TITLE );
	window.setFramerateLimit( ivy::FRAME_RATE_LIMIT );
	window.setKeyRepeatEnabled(false);

	// init screen manager
	ivy::ScreenManager manager;
	manager.setWindow( &window );

	// add first screen
	manager.addScreen( ivy::GAME_INTRO );

	// create global timer
	sf::Clock clock;

	// game loop
	while ( window.isOpen() )
	{
		// get elapsed time so we can update the
		// game with more accuracy
		sf::Time time = clock.getElapsedTime();
		sf::Int32 time_elapsed = time.asMilliseconds();

		// restart clock
		clock.restart();

		// std::cout << "time :" << time.asMilliseconds() << "\n";

		// update all screen states
		manager.update( time_elapsed );

		// clear the screen
		window.clear();

		// draw all screens
		manager.draw();

		// render the screen
		window.display();
	}

	return 0;
}