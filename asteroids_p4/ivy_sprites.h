#ifndef IVY_SPRITES_H
#define IVY_SPRITES_H

#include <vector>
using namespace std;

#include <SFML\Graphics.hpp>
#include "ivy_enums.h"

namespace ivy{

	// to-do
	// load sprite resources in threads
	// use flags to signal that a sprite is done loading resources
	// make sure screens check the sprite flags for done loading before changing state from SCREEN_INIT to SCREEN_TRANSITION_IN
	
	// ==================================================
	// 
	// sprites
	// 
	// ==================================================

	class Sprite{
	protected:
		static int ID_COUNTER;
		int sprite_id;
		sf::Sprite sprite; // current sprite being drawn at x/y position with w/h size
		sf::RectangleShape hitbox_visual;

		sf::Rect<float> hitbox; // hitbox based on position/size
		sf::Vector2f position; // the origin position
		sf::Vector2f size; // the raw width/height
		sf::Rect<int> draw_rect; // drawable area inside spritesheet
		sf::Vector2f move_vector; // movement vector (magnitude and direction)

		// movement related
		enum MoveState{
			MOVE_FORWARD,
			MOVE_BACKWARD,
			MOVE_FREE
		} move_state;
		enum TurnState{
			TURN_LEFT,
			TURN_RIGHT,
			TURN_FREE
		} turn_state;
		float accel_speed;
		float decel_speed;
		float turn_speed;
		float max_speed;

		// animation related
		float anim_frame_elapsed; // how long this frame was displaye
		float anim_frame_speed; // changes based on state sometimes
		int anim_frame_start; // starting point for animation
		int anim_frame; // changes every update
		int anim_frame_max; // changes based on state

		sf::Vector2f transformPoint( sf::Vector2f p1, sf::Vector2f p2, float angle ){
			float rads = float(angle / 180.f * ivy::PI);
			float x = p1.x + (p2.x - p1.x) * std::cos(-rads) + (p2.y - p1.y) * std::sin(-rads);
			float y = p1.y - (p2.x - p1.x) * std::sin(-rads) + (p2.y - p1.y) * std::cos(-rads);
			return sf::Vector2f( x, y );
		}
		sf::Vector2f getVector( float magnitude, float angle ){
			float rads = float(angle / 180.f * ivy::PI);
			sf::Vector2f point = sf::Vector2f( 0, -magnitude );
			float x = point.x * std::cos(-rads) + point.y * std::sin(-rads);
			float y = point.x * std::sin(-rads) + point.y * std::cos(-rads);
			return sf::Vector2f( x, y );
		}
		void drawHitBox( sf::RenderWindow *window ){
			// for debugging
			hitbox_visual.setPosition( hitbox.left, hitbox.top );
			hitbox_visual.setSize( sf::Vector2f(hitbox.width, hitbox.height) );
			hitbox_visual.setFillColor( sf::Color( 255, 0, 0, 255 ) );
			(*window).draw( hitbox_visual );
		}
		void updateRotate( float delta ){
			float old_angle = sprite.getRotation();
			float new_angle = 0.f;
			switch ( turn_state ){
				case TURN_LEFT:
					new_angle = -(delta * turn_speed);
					break;
				case TURN_RIGHT:
					new_angle = (delta * turn_speed);
					break;
				case TURN_FREE:
				default:
					break;
			}
			float final_angle = old_angle + new_angle;
			sprite.setRotation( final_angle );
		}
		void updatePosition( float delta ){

			// ==================================================
			// 
			// check movement parameters first
			// 
			// ==================================================

			float new_speed = 0.f;
			bool moved = false;
			switch ( move_state ){
				case MOVE_FORWARD:
					new_speed = (delta * accel_speed);
					moved = true;
					break;
				case MOVE_BACKWARD:
					new_speed = -(delta * decel_speed);
					moved = true;
					break;
				case MOVE_FREE:
				default:
					break;
			}

			// ==================================================
			// 
			// calculate movement vector, then update position
			// 
			// ==================================================
			
			// final position to be calculated
			float x = position.x;
			float y = position.y;

			if (moved){
				// if the ship moved in a different direction,
				// add the new movement vector to current movement
				float final_angle = sprite.getRotation();
				sf::Vector2f new_vector = getVector( new_speed, final_angle );
				move_vector += new_vector;
				// std::cout << "pos :" << new_vector.x << "," << new_vector.y << " / " << move_vector.x << "," << move_vector.y << "\n";
			}
			float cx = move_vector.x;
			float cy = move_vector.y;

			// cap the vector's magnitude
			float curr_speed = std::sqrt((cx*cx)+(cy*cy));
			if (curr_speed > max_speed){
				move_vector.x *= (max_speed / curr_speed);
				move_vector.y *= (max_speed / curr_speed);
			}
			float new_x = (delta * move_vector.x);
			float new_y = (delta * move_vector.y);
			x += new_x;
			y += new_y;

			// ==================================================
			// 
			// wrap ship movement within screen
			// 
			// ==================================================

			// if the ship moved out of screen bounds, wrap it
			// NOTE :
			// the ship's top/left are already centered because we
			// setOrigin() it to the center, but not the globalBounds.
			sf::Rect<float> bounds = sprite.getGlobalBounds();
			float center_x = bounds.left + (bounds.width/2.f);
			float center_y = bounds.top + (bounds.height/2.f);
			if ( center_y < 0 ){
				y = ivy::WINDOW_H + new_y;
			}
			if ( center_y > ivy::WINDOW_H ){
				y = + new_y;
			}
			if ( center_x < 0 ){
				x = ivy::WINDOW_W + new_x;
			}
			if ( center_x > ivy::WINDOW_W ){
				x = new_x;
			}

			// ==================================================
			// 
			// finalize the ship's position
			// 
			// ==================================================

			setPosition( x, y );
		}
		void updateAnimation( sf::Int32 time_elapsed ){
			anim_frame_elapsed += time_elapsed;
			if ( anim_frame_elapsed > anim_frame_speed ){
				// get the remainder of time and add it to next anim cycle
				anim_frame_elapsed = fmod(anim_frame_elapsed, anim_frame_speed);
				// cycle frame counter
				anim_frame = (++anim_frame) % anim_frame_max;
				// move draw_rect to next frame
				draw_rect.left = anim_frame_start + (anim_frame * draw_rect.width);
				// update texture position
				sprite.setTextureRect( draw_rect );
			}
		}
	public:
		// get / set

		void setSpritesheet( sf::Texture *ss ){
			sprite.setTexture( (*ss) );
			sprite.setTextureRect( draw_rect );
		}
		void setPosition( float x, float y ){
			// update the x/y position of sprite
			position.x = x;
			position.y = y;
			// update hitbox based on the position, centered
			hitbox.left = (x - hitbox.width/2.f);
			hitbox.top = (y - hitbox.height/2.f);
			// update the sprite position
			sprite.setPosition( position.x, position.y );
		}
		virtual sf::Rect<float> getHitBox(){
			return hitbox;
		}

		// update / draw

		virtual void update( sf::Int32 time_elapsed ){
			// all event handles are done in parent screen.
			// the method here only handles passive
			// updates, such as sprite animation.

			// get modifier based on time elapsed
			float delta = time_elapsed / 1000.f;

			// rotate the sprite
			updateRotate( delta );

			// update the sprite position, wrap around screen
			updatePosition( delta );

			// update sprite animation
			updateAnimation( time_elapsed );
		}
		virtual void draw( sf::RenderWindow *window ){
			// every sprite should only just draw from window.
			// the parent screen handles drawing everything in
			// one loop.

			// draw this sprite
			(*window).draw( sprite );
		}

		// positive movement

		void accelerate(){
			// keep adding velocity until stopAccelerate is called
			move_state = MOVE_FORWARD;
		}
		void decelerate(){
			// keep reducing velocity until stopDecelerate is called
			move_state = MOVE_BACKWARD;
		}
		void turnLeft(){
			// keep turning until stopTurn is called
			turn_state = TURN_LEFT;
		}
		void turnRight(){
			// keep turning until stopTurn is called
			turn_state = TURN_RIGHT;
		}

		// stop movement

		void stopAccelerate(){
			move_state = MOVE_FREE;
		}
		void stopDecelerate(){
			move_state = MOVE_FREE;
		}
		void stopTurn(){
			turn_state = TURN_FREE;
		}

		// constructor / destructor

		Sprite(){
			cout << "+ Sprite\n";

			// default stats
			move_state = MOVE_FREE;
			turn_state = TURN_FREE;
			accel_speed = 0.f; // bullets default at max velocity
			decel_speed = 0.f; // bullets can't slow down
			turn_speed = 0.f; // bullets can't turn
			max_speed = 0.f; // maximum velocity

			// default position of sprite is at 0,0 of screen
			position.x = 0;
			position.y = 0;

			// default hitbox
			hitbox.left = 0;
			hitbox.top = 0;
			hitbox.width = 5;
			hitbox.height = 5;

			// default origin is on 0,0 of spritesheet
			draw_rect.left = 0;
			draw_rect.top = 0;

			// animation stats
			anim_frame = 0;
			anim_frame_start = 0;
			anim_frame_elapsed = 0;
			anim_frame_max = 1; // how many frames for current animation loop
			anim_frame_speed = 200; // 200ms per frame; 5 frames per second

			// default no movement
			move_vector.x = 0;
			move_vector.y = 0;
		}
		virtual ~Sprite(){
			cout << "- Sprite\n";
		}
	};

	class Sprite_Bullet : public Sprite{
	private:
		enum BulletState{
			BULLET_INIT = 0,
			BULLET_MOVE,
			BULLET_HIT
		} bullet_state;
		float distance;
		const float max_distance; // in pixels
	public:
		// get / set

		void setBulletState( BulletState s ){
			bullet_state = s;
			switch (bullet_state){
				case BULLET_INIT:
				case BULLET_MOVE:
				case BULLET_HIT:
					// starting frame position in spritesheet
					anim_frame_start = 40;
					draw_rect.left = 40;
					draw_rect.top = 24;
					break;
			}
		}

		// actions

		void initBullet( sf::Vector2f origin, float angle ){
			// make this bullet "alive"
			setBulletState( BULLET_MOVE );

			// init the start position
			setPosition( origin.x, origin.y );

			// set the rotation
			sprite.setRotation( angle );

			cout << "### setting bullet : " << position.x << "," << position.y << "," << angle << "\n";
		}
		bool isBulletFree(){
			if ( bullet_state == BULLET_INIT ){
				return true;
			}
			else{
				return false;
			}
		}

		// update / draw

		void update( sf::Int32 time_elapsed ){
			// if state is moving, update position.
			if ( bullet_state != BULLET_INIT ){
				Sprite::update( time_elapsed );

				float delta = time_elapsed / 1000.f;
				float cx = move_vector.x;
				float cy = move_vector.y;
				float curr_speed = std::sqrt((cx*cx)+(cy*cy));
				float curr_distance = delta * curr_speed;
				distance += curr_distance;

				if ( distance > max_distance ){
					setBulletState( BULLET_INIT );
					distance = 0;
				}
			}

			// to-do
			// get total distance travelled
			// check if distance > max_distance
			// if over, reset/hide bullet and set it to init
		}
		void draw( sf::RenderWindow *window ){
			// if state is moving, draw.
			if ( bullet_state != BULLET_INIT ){
				Sprite::draw( window );
				// drawHitBox( window );
			}
		}

		// constructor / destructor

		Sprite_Bullet() : max_distance(600) {
			cout << "+ Sprite_Bullet\n";

			// default specific
			setBulletState( BULLET_INIT );

			// default stats
			move_state = MOVE_FORWARD;
			accel_speed = 400000.f;
			max_speed = 400.f; // maximum velocity // temp!
			// max speed in 400/400000 = 0.001 seconds

			// bullet travelled distance
			distance = 0;

			// hitbox
			hitbox.width = 2;
			hitbox.height = 2;

			// default origin is on 0,0 of spritesheet
			draw_rect.left = 40;
			draw_rect.top = 24;

			// drawable size
			draw_rect.width = 4;
			draw_rect.height = 4;

			// animation stats
			anim_frame_max = 2; // 2 frames for bullet animation

			// set the origin point
			float origin_x = draw_rect.width / 2.f;
			float origin_y = draw_rect.height / 2.f;
			sprite.setOrigin( origin_x, origin_y );
		}
		~Sprite_Bullet(){
			cout << "- Sprite_Bullet\n";
		}
	};

	// NOTE
	// ships keep track of how many bullets it can shoot,
	// so the bullet class has to be defined first.

	class Sprite_Ship : public Sprite{
	private:
		sf::Texture bullet_spritesheet; // right now it's the same full spritesheet as the ship
		enum ShipState{
			SHIP_INIT = 0,
			SHIP_SPAWN,
			SHIP_ALIVE,
			SHIP_EXPLODE,
			SHIP_DEAD
		} ship_state;
		bool is_shooting;
		int life;
		const int max_life;
		int bullets;
		const int max_bullets;
		float shoot_cooldown;
		const float max_shoot_cooldown;
		std::vector<ivy::Sprite_Bullet*> _bullets;

		ivy::Sprite_Bullet* getFreeBullet(){
			std::vector<ivy::Sprite_Bullet*>::iterator it;
			for (it = _bullets.begin(); it != _bullets.end(); it++){
				if ( (*it)->isBulletFree() ){
					return (*it);
					break;
				}
			}
			return NULL;
		}
		void shootBullet(){
			// if cooldown is over, allow shoot next bullet
			if ( shoot_cooldown <= 0 ){
				// find a bullet that isn't in use
				ivy::Sprite_Bullet *bullet = getFreeBullet();
				if ( bullet != NULL ){
					std::cout << "PEW\n";

					float angle = sprite.getRotation();
					bullet->initBullet( position, angle );
				}
				else{
					// if we used up all our bullets, don't shoot anymore
					cout << "NO FREE BULLET\n";
				}
			}
		}
		ivy::Sprite_Bullet* createBullet(){
			// creates the bullet to be collected from the game screen
			
			// add a bullet to the ship's bullet list
			// the game screen will poll for any available
			// bullets each cycle.
		}
	public:
		void setShipState( ShipState s ){
			ship_state = s;
			switch (ship_state){
				case SHIP_INIT:
				case SHIP_ALIVE:
					anim_frame_start = 32;
					draw_rect.left = 32;
					draw_rect.top = 16;
					break;

				case SHIP_SPAWN:
					break;

				case SHIP_EXPLODE:
					break;

				case SHIP_DEAD:
					break;
			}
		}

		// actions

		void addLife(){
			--life;
		}
		void minusLife(){
			++life;
			if (life <= 0){
				setShipState( SHIP_DEAD );
			}
		}
		void startShoot(){
			is_shooting = true;
		}
		void stopShoot(){
			is_shooting = false;
		}

		// update / draw

		void update( sf::Int32 time_elapsed ){

			// update the base rotation / movement
			Sprite::update( time_elapsed );

			// get modifier based on time elapsed
			float delta = time_elapsed / 1000.f;

			// ==================================================
			// 
			// update bullet shooting
			// 
			// ==================================================

			// if shoot key is held, keep trying to shoot
			if ( shoot_cooldown > 0 ){
				// if we're in cooldown, don't shoot regardless of keypress
				shoot_cooldown -= time_elapsed;
			}
			else{
				// cooldown over. If key pressed, shoot bullet.
				if ( is_shooting ){
					shootBullet();
					shoot_cooldown = max_shoot_cooldown;
				}
			}

			// ==================================================
			// 
			// update bullet positions
			// 
			// ==================================================

			// NOTE: bullet collision should be done in game screen
			// layer, so that we can compare with other objects
			// on the same level, e.g. rocks

			if (_bullets.size() > 0){
				std::vector<Sprite_Bullet*>::iterator i;
				for (i = _bullets.begin(); i != _bullets.end(); ++i){
					(*i)->update( time_elapsed );
				}
			}
		}
		void draw( sf::RenderWindow *window ){

			// draw itself before drawing other things
			Sprite::draw( window );

			// draw the bullets
			if (_bullets.size() > 0){
				std::vector<Sprite_Bullet*>::iterator i;
				for (i = _bullets.begin(); i != _bullets.end(); ++i){
					(*i)->draw( window );
				}
			}

			// for debugging only
			drawHitBox( window );
		}

		// constructor / destructor

		Sprite_Ship() : max_life(5), max_bullets(6), max_shoot_cooldown(100) {
			cout << "+ Sprite_Ship\n";

			// load the spritesheet for bullet
			if ( !bullet_spritesheet.loadFromFile( "assets/main.png" ) ){
				throw 0;
			}
			// add color masking - set #FF00FF color as transparent
			sf::Image image = bullet_spritesheet.copyToImage();
			image.createMaskFromColor( sf::Color(255,0,255), 0 );
			bullet_spritesheet.loadFromImage( image );

			// specific stats
			is_shooting = false;
			life = 3; // if user is lucky, they can earn more than default 3 lives
			shoot_cooldown = max_shoot_cooldown; // how fast we can shoot each bullet

			// create a fixed list of bullets
			for (int i = max_bullets; i--;){
				ivy::Sprite_Bullet *bullet = new ivy::Sprite_Bullet();
				(*bullet).setSpritesheet( &bullet_spritesheet );
				_bullets.push_back( bullet );
				cout << "CREATED BULLET\n";
			}

			// default stats
			accel_speed = 800.f; // add velocity by 800px/s
			decel_speed = 800.f;
			turn_speed = 180.f;
			max_speed = 400.f; // maximum velocity
			// thus, we reach max speed in 0.5 seconds (400 / 800)

			// default origin is on 0,0 of spritesheet
			draw_rect.left = 0;
			draw_rect.top = 0;

			// ship hitbox
			hitbox.width = 4;
			hitbox.height = 4;

			// ship drawable size
			draw_rect.width = 8;
			draw_rect.height = 16;

			// set the origin point
			float origin_x = draw_rect.width / 2.f;
			float origin_y = draw_rect.height / 2.f;
			sprite.setOrigin( origin_x, origin_y );

			// update state
			setShipState( SHIP_INIT );
		}
		~Sprite_Ship(){
			cout << "- Sprite_Ship\n";

			// NOTE
			// vector's erase only removes the pointer, but the object still exists
			// we need to manually call delete to actually delete the object

			std::vector<ivy::Sprite_Bullet*>::iterator it;
			for (it = _bullets.begin(); it != _bullets.end(); it++){
				delete (*it);
			}
			_bullets.clear();
		}
	};

	class Sprite_Rock : public Sprite{
	private:
	public:
		Sprite_Rock(){
			cout << "+ Sprite_Rock\n";
		}
		~Sprite_Rock(){
			cout << "- Sprite_Rock\n";
		}
	};
}

#endif