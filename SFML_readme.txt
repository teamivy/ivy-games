Newbie guide:

1) download and install Microsoft Visual Studio a.k.a. MVS (2010 or newer)

2) get SFML from http://www.sfml-dev.org/download.php (as of writing, get the "SFML 2.1" stable version)

3) Extract SFML files into C:/ to make it easier for you to link to the library files whenever you create a new project.

4) To start a new project, open MVS, go to File > New > Project.

5) Pick "Empty Project", give it a name and choose your folder location, then click "OK".

6) We need to link SFML files for this project, so... Go to Project > Properties

7) On the top left corner, under "Configuration" dropdown, are different environment settings.

7.1) Active(Debug) and Debug are settings for debugging environment.
7.1.1) Release is for the production environment (for releasing to the public usage).
7.1.2) All Configuration is settings for both Debug/Release environment. You can do most of your settings here, but when some settings cannot be changed, you need to manually set them in the Debug or Release settings environment.

7.2) To link SFML (or SDL files, it's similar process), we need 3 things:
7.2.1) include files (found in the "include" folder)
7.2.2) library files (found in the "lib" folder)
7.2.3) binary header files (found in the "bin" folders)

7.3) On the left pane, go to Configuration Properties > VC++ Directories. 

7.3.1) In the "Include Directories" section, add the following path:
C:\SFML\include

7.3.1) In the "Library Directories" section, add the following path:
C:\SFML\lib

Note: the path varies depending on where your SFML include/lib folders are.

Now we have added the necessary SFML files, we can use the library when we code later. But before we can actually "use" SFML, we need to link it...

7.4) On the left pane, go to Configuration Properties > Linker > Input. In "Additional Dependencies", edit it based on the following:

7.4.1) if you are in Active(Debug) or Debug environment, add the following:
sfml-graphics-d.lib
sfml-window-d.lib
sfml-system-d.lib

7.4.2) if you are in Release environment, add the following:
sfml-graphics.lib
sfml-window.lib
sfml-system.lib

As you can see, the only difference is the "-d" in the filename, to indicate that the library file is used for debug or release environments.

Now we have linked the SFML libraries, we can start programming!

8) When compiling the project, you most probably will get error message like this:

"The program can't start because sfml-graphics-d-2.dll is missing from your computer. Try reinstalling the program to fix this problem."

It's not a big problem, it just means it cannot find the dll files that are necessary to "build" your game.

To fix it, go to your SFML bin folder ...
e.g. C:\SFML\bin
... and copy ALL the dll files into your project's main folder
e.g. C:\Users\Greg\Desktop\IVY_GAMES\SFML_tictactoe\SFML_tictactoe

To be sure, it should be where your code files are located, e.g. main.cpp

9) That should be it. If you have any other problems or issues, make sure you followed step (7) thoroughly. If problems still persist, then it's probably not related to setting up your project.



NOTES:
- instead of using absolute paths e.g. "C:\My_Project\folder", apparently we can use "$(ProjectDir)\folder" instead, so that our paths always point from our project.

