#include <SFML/Graphics.hpp>
#include "ivy_enums.h"
#include "ivy_globals.h"
#include "ivy_screens.h"

int main(){

	// Create the main window
    sf::RenderWindow window( sf::VideoMode( ivy::WINDOW_W, ivy::WINDOW_H ), "Team Pong" );
	window.setFramerateLimit(60);
	window.setKeyRepeatEnabled(false);

	// load font, sprites
	ivy::init_resources();

	// first time setup screen variables
	ivy::init_screens();

	// set initial game state
	ivy::GameState state = ivy::MENU;

	// game loop
    while ( window.isOpen() )
    {
		// update game
		switch( state ){
		case ivy::MENU:
			ivy::update_menu( &window, &state );
			break;
		case ivy::GAME:
			ivy::update_game( &window, &state );
			break;
		case ivy::SCORE:
			ivy::update_score( &window, &state );
			break;
		case ivy::QUIT:
			window.close();
			break;
		default:
			break;
		}

        // clear screen
        window.clear();

		// draw to screen
		switch( state ){
		case ivy::MENU:
			ivy::draw_menu( &window );
			break;
		case ivy::GAME:
			ivy::draw_game( &window );
			break;
		case ivy::SCORE:
			ivy::draw_score( &window );
			break;
		default:
			break;
		}

        // render screen
        window.display();
    }

	return EXIT_SUCCESS;
}