#ifndef IVY_ENUMS_H
#define IVY_ENUMS_H

namespace ivy{
	enum GameState{
		QUIT = 0,
		MENU,
		GAME,
		SCORE
	};
}

#endif