#include <SFML/Graphics.hpp>
#include "ivy_enums.h"
#include "ivy_screens.h"
#include <iostream>
#include <sstream>
using namespace std;

namespace ivy{
	// resources
	sf::Font font;
	sf::RectangleShape s_board;
	sf::RectangleShape s_player_1;
	sf::RectangleShape s_player_2;
	sf::CircleShape s_ball;

	// menu
	char* _menu_items[2];
	int menu_hl_selected;
	int menu_hl_alpha;
	int menu_hl_alpha_dir;

	// game
	sf::Rect<float> player_1;
	sf::Rect<float> player_2;
	sf::Rect<float> ball;
	sf::Rect<float> board;
	float player_speed; // constant
	int ball_x_speed;
	int ball_y_speed;
	int ball_x_velocity; // -ve = left, +ve = right
	int ball_y_velocity; // -ve = up, +ve = down
	bool started; // whether the game has started
	int player_start; // which player starts (1 = left, 2 = right)
	int winner;
	int wins;
	int loses;
	bool pressed_up;
	bool pressed_down;

	// score
	char* _score_items[2];
	int score_hl_selected;
	int score_hl_alpha;
	int score_hl_alpha_dir;

	// ==================================================
	// first time run only
	// ==================================================

	void init_resources(){
		if ( !font.loadFromFile("assets/georgia.ttf") ){
			throw 0;
		}

		// game object properties
		ball_x_speed = 4;
		ball_y_speed = 4;

		board.top = 50;
		board.left = 50;
		board.width = 600;
		board.height = 400;

		player_1.width = 10;
		player_1.height = 50;

		player_2.width = 10;
		player_2.height = 50;

		ball.width = 10;
		ball.height = 10;

		s_board.setFillColor( sf::Color::Color( 0, 0, 255, 50 ) );
		s_player_1.setFillColor( sf::Color::Color( 255, 0, 0 ) );
		s_player_2.setFillColor( sf::Color::Color( 255, 0, 0 ) );
		s_ball.setFillColor( sf::Color::Color( 0, 255, 0 ) );

		s_board.setSize( sf::Vector2f( board.width, board.height ) );
		s_player_1.setSize( sf::Vector2f( player_1.width, player_1.height ) );
		s_player_2.setSize( sf::Vector2f( player_2.width, player_2.height ) );
		s_ball.setRadius( ball.width / 2 );
	}
	void init_screens(){
		reset_menu();
		reset_game();
		reset_score();
	}

	// ==================================================
	// use to reset variables before entering the screen
	// ==================================================

	void reset_menu(){
		_menu_items[0] = "Start Pong";
		_menu_items[1] = "Quit";
		menu_hl_selected = 0;
		menu_hl_alpha = 255;
		menu_hl_alpha_dir = -5;
	}
	void reset_game(){
		// player 1 is on top-left
		player_1.top = board.top;
		player_1.left = board.left;

		// player 2 is on bottom-right
		player_2.top = board.top + board.height - player_2.height;
		player_2.left = board.left + board.width - player_2.width;

		// ball position
		ball.top = (board.top + board.height) / 2;
		ball.left = (board.left + board.width) / 2;

		// if not started, stick ball to current player
		started = false;
		pressed_up = false;
		pressed_down = false;
	}
	void reset_score(){
		_score_items[0] = "Play Again";
		_score_items[1] = "Back to Menu";
		score_hl_selected = 0;
		score_hl_alpha = 255;
		score_hl_alpha_dir = -5;
	}

	void start_round( int side = 1 ){
		reset_game();

		// reset speed and variables
		player_speed = 5;
		player_start = side; // 1 = left, 2 = right

		if (side == 1){
			// from left, default move right/down
			ball_x_velocity = ball_x_speed;
			ball_y_velocity = ball_y_speed;
		}
		else{
			// from right, default move up/left
			ball_x_velocity = -ball_x_speed;
			ball_y_velocity = -ball_y_speed;
		}
	}
	int hit_player(){

		if (ball.left < player_1.left + player_1.width){
			// as long as the ball passed behind player_1
			if (ball.top < player_1.top + player_1.height &&
				ball.top + ball.height > player_1.top)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else if (ball.left + ball.width > player_2.left){
			// as long as the ball passed behind player_2
			if (ball.top < player_2.top + player_2.height &&
				ball.top + ball.height > player_2.top)
			{
				return 2;
			}
			else
			{
				return 0;
			}
		}
		else{
			return 0;
		}
	}
	int hit_bounds(){
		if (ball.top < board.top){
			// hit the top of the board
			return 1;
		}
		else if (ball.top + ball.height > board.top + board.height){
			// hit the bottom of the board
			return 2;
		}
		else{
			return 0;
		}
	}
	int hit_win_area(){
		// NOTE
		// allow buffer space before the ball qualifies
		// as fully entering the win/lose area so that there
		// is time for the ball to be deflected if it hit
		// the player.

		float buffer = 10.f;
		if (ball.left < board.left - buffer){
			// hit the left of the board
			return 1;
		}
		else if (ball.left + ball.width > board.left + board.width + buffer){
			// hit the right of the board
			return 2;
		}
		else{
			return 0;
		}
	}

	// ==================================================
	// update and draw for each screen
	// ==================================================

	void update_menu( sf::RenderWindow *window, ivy::GameState *state ){
		sf::Event e;

		while ( (*window).pollEvent(e) ){
			// close game if window X is pressed
			if ( e.type == sf::Event::Closed ){
				(*window).close();
			}

			// key press events
			if ( e.type == sf::Event::KeyPressed ){
				// get key code
				sf::Keyboard::Key key = e.key.code;

				if ( key == sf::Keyboard::Up ){
					menu_hl_selected--;
				}
				if ( key == sf::Keyboard::Down ){
					menu_hl_selected++;
				}
				if ( key == sf::Keyboard::Return ){
					switch (menu_hl_selected){
						case 0: {
							start_round( 1 );
							(*state) = ivy::GAME;
						}
						break;

						case 1:{
							(*state) = ivy::QUIT;
						}
						break;
					}
				}
			}
		}

		// cap highlight position
		if ( menu_hl_selected < 0 ){
			menu_hl_selected = 0;
		}
		if ( menu_hl_selected > 1 ){
			menu_hl_selected = 1;
		}

		// update glow effect
		menu_hl_alpha += menu_hl_alpha_dir;
		if ( menu_hl_alpha < 50 ){
			menu_hl_alpha = 50;
			menu_hl_alpha_dir *= -1;
		}
		if ( menu_hl_alpha > 255 ){
			menu_hl_alpha = 255;
			menu_hl_alpha_dir *= -1;
		}
	}
	void draw_menu( sf::RenderWindow *window ){
		for (int i = 0; i < 2; i++){
			// for each text...
			const char* str = _menu_items[i];
			sf::Text text( str, font, 50 );
			sf::Color color;

			// if selected, glow red
			// if not, just white
			if ( i == menu_hl_selected ){
				color = sf::Color( 255, 0, 0, menu_hl_alpha );
			}
			else{
				color = sf::Color::White;
			}

			// set position/color
			float x = 50;
			float y = float( i * 60 + 50 );
			text.setPosition( x, y );
			text.setColor( color );

			// draw
			(*window).draw( text );
		}
	}

	void update_game( sf::RenderWindow *window, ivy::GameState *state ){

		// update movement
		sf::Event e;
		while( (*window).pollEvent(e) ){
			// close game if window X is pressed
			if ( e.type == sf::Event::Closed ){
				(*window).close();
			}

			if ( e.type == sf::Event::KeyPressed ){
				// get key code
				sf::Keyboard::Key key = e.key.code;

				// get movement
				if ( key == sf::Keyboard::Up ){
					pressed_up = true;
				}
				if ( key == sf::Keyboard::Down ){
					pressed_down = true;
				}

				// launch the ball if it hasn't yet
				if ( key == sf::Keyboard::Return ){
					started = true;
				}
			}

			if ( e.type == sf::Event::KeyReleased ){
				// get key code
				sf::Keyboard::Key key = e.key.code;

				// get movement
				if ( key == sf::Keyboard::Up ){
					pressed_up = false;
				}
				if ( key == sf::Keyboard::Down ){
					pressed_down = false;
				}
			}
		}

		// move player
		if ( pressed_up ){
			player_1.top -= player_speed;
		}
		if ( pressed_down ){
			player_1.top += player_speed;
		}
		// make sure the players don't move out of the board
		if ( player_1.top < board.top ){
			player_1.top = board.top;
		}
		if ( player_1.top + player_1.height > board.top + board.height ){
			player_1.top = board.top + board.height - player_1.height;
		}

		if ( started ){
			// move the ball
			ball.left += ball_x_velocity;
			ball.top += ball_y_velocity;

			// check collision
			int ball_hit = hit_player();
			if ( ball_hit ){
				cout << "hit player : " << ball_hit << "\n";
				if ( ball_hit == 1 ){
					ball_x_velocity = ball_x_speed;
				}
				if ( ball_hit == 2 ){
					ball_x_velocity = -ball_x_speed;
				}
			}

			int ball_bounce = hit_bounds();
			if ( ball_bounce ){
				cout << "hit wall : " << ball_bounce << "\n";
				if ( ball_bounce == 1 ){
					ball_y_velocity = ball_y_speed;
				}
				if ( ball_bounce == 2 ){
					ball_y_velocity = -ball_y_speed;
				}
			}

			// based on where the ball was, move AI in that direction
			if ( player_2.top + (player_2.height/2) > ball.top + (ball.height/2) ){
				// purposely make AI slower so we can eventually win
				player_2.top -= (player_speed - 1.5);
			}
			if ( player_2.top + (player_2.height/2) < ball.top + (ball.height/2) ){
				// purposely make AI slower so we can eventually win
				player_2.top += (player_speed - 1.5);
			}
			// make sure the player don't move out of the board
			if ( player_2.top < board.top ){
				player_2.top = board.top;
			}
			if ( player_2.top + player_2.height > board.top + board.height ){
				player_2.top = board.top + board.height - player_2.height;
			}

			// check winning conditions
			int ball_win = hit_win_area();
			if (ball_win){
				cout << "win area : " << ball_win << "\n";
				reset_score();
				(*state) = ivy::SCORE;
				if ( ball_win == 1 ){
					// ball went to the left side
					loses++;
					winner = 2;
				}
				else{
					// ball went to the right side
					wins++;
					winner = 1;
				}
			}
		}
		else{
			// if the game hasn't started, just
			// move the ball according to where the player is

			// depending on whose turn it is, stick the
			// ball to the correct player and set ball direction
			if ( player_start == 1 ){
				ball.top = player_1.top + (player_1.height / 2) - (ball.height / 2);
				ball.left = player_1.left + player_1.width;
			}
			else{
				ball.top = player_2.top + (player_2.height / 2) - (ball.height / 2);
				ball.left = player_2.left - ball.width;
			}

			// NOTE
			// there should be a handling for when the game
			// starts with the AI instead, so we need to tell
			// the AI to start the game... but for simplicity
			// we always start the game on player side.
		}
	}
	void draw_game( sf::RenderWindow *window ){
		// draw the board area
		s_board.setPosition( board.left, board.top );
		(*window).draw( s_board );

		// draw the players
		s_player_1.setPosition( player_1.left, player_1.top );
		(*window).draw( s_player_1 );

		s_player_2.setPosition( player_2.left, player_2.top );
		(*window).draw( s_player_2 );

		// draw the ball
		s_ball.setPosition( ball.left, ball.top );
		(*window).draw( s_ball );
	}

	void update_score( sf::RenderWindow *window, ivy::GameState *state ){
		sf::Event e;

		while ( (*window).pollEvent(e) ){
			// close game if window X is pressed
			if ( e.type == sf::Event::Closed ){
				(*window).close();
			}

			// key press events
			if ( e.type == sf::Event::KeyPressed ){
				// get key code
				sf::Keyboard::Key key = e.key.code;

				if ( key == sf::Keyboard::Up ){
					score_hl_selected--;
				}
				if ( key == sf::Keyboard::Down ){
					score_hl_selected++;
				}
				if ( key == sf::Keyboard::Return ){
					switch (score_hl_selected){
						case 0: {
							start_round( 1 );
							(*state) = ivy::GAME;
						}
						break;

						case 1:{
							reset_menu();
							(*state) = ivy::MENU;
						}
						break;
					}
				}
			}
		}

		// cap highlight position
		if ( score_hl_selected < 0 ){
			score_hl_selected = 0;
		}
		if ( score_hl_selected > 1 ){
			score_hl_selected = 1;
		}

		// update glow effect
		score_hl_alpha += score_hl_alpha_dir;
		if ( score_hl_alpha < 50 ){
			score_hl_alpha = 50;
			score_hl_alpha_dir *= -1;
		}
		if ( score_hl_alpha > 255 ){
			score_hl_alpha = 255;
			score_hl_alpha_dir *= -1;
		}
	}
	void draw_score( sf::RenderWindow *window ){
		// draw score message
		char* score_str;
		sf::Color score_color;
		if (winner == 1){
			score_str = "You won!";
			score_color.g = 255;
		}
		else if (winner == 2){
			score_str = "You lost...";
			score_color.r = 255;
		}
		else{
			score_str = "No message";
			score_color.b = 255;
		}
		sf::Text score_text( score_str, font, 35 );
		score_text.setColor( score_color );
		score_text.setPosition( 100, 250 );
		(*window).draw( score_text );

		// draw score stats
		std::ostringstream stat_str;
		stat_str << "Wins : " << wins << ", Loses : " << loses;
		sf::Text stat_text( stat_str.str(), font, 35 );
		stat_text.setColor( sf::Color::White );
		stat_text.setPosition( 100, 350 );
		(*window).draw( stat_text );

		for (int i = 0; i < 2; i++){
			// for each text...
			const char* str = _score_items[i];
			sf::Text text( str, font, 50 );
			sf::Color color;

			// if selected, glow red
			// if not, just white
			if ( i == score_hl_selected ){
				color = sf::Color( 255, 0, 0, score_hl_alpha );
			}
			else{
				color = sf::Color::White;
			}

			// set position/color
			float x = 50;
			float y = float( i * 60 + 50 );
			text.setPosition( x, y );
			text.setColor( color );

			// draw
			(*window).draw( text );
		}
	}
}