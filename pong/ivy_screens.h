#ifndef IVY_SCREENS_H
#define IVY_SCREENS_H

namespace ivy{

	void init_resources();
	void init_screens();

	void reset_menu();
	void reset_game();
	void reset_score();

	void start_round();
	int hit_player();
	int hit_bounds();
	int hit_win_area();

	void update_menu( sf::RenderWindow *window, ivy::GameState *state );
	void draw_menu( sf::RenderWindow *window );

	void update_game( sf::RenderWindow *window, ivy::GameState *state );
	void draw_game( sf::RenderWindow *window );

	void update_score( sf::RenderWindow *window, ivy::GameState *state );
	void draw_score( sf::RenderWindow *window );
}

#endif