#ifndef IVY_SPRITES_H
#define IVY_SPRITES_H

#include <vector>
using namespace std;

#include <SFML\Graphics.hpp>
#include "ivy_enums.h"

namespace ivy{

	// to-do
	// load sprite resources in threads
	// use flags to signal that a sprite is done loading resources
	// make sure screens check the sprite flags for done loading before changing state from SCREEN_INIT to SCREEN_TRANSITION_IN
	
	// ==================================================
	// 
	// sprites
	// 
	// ==================================================

	class Sprite{
	protected:
		static int ID_COUNTER;
		int sprite_id;
		sf::Sprite sprite; // current sprite being drawn at x/y position with w/h size

		sf::Rect<float> bound_rect; // actual position/bounds of this sprite
		sf::Rect<int> draw_rect; // drawable area inside spritesheet
		sf::Vector2f move_vector; // movement vector (magnitude and direction)
		float move_angle;
	public:
		virtual void setSpritesheet( sf::Texture *ss ){
			sprite.setTexture( (*ss) );
		}
		virtual void setPosition( float x, float y ){
			// update the x/y position of sprite
			bound_rect.left = x;
			bound_rect.top = y;
		}
		virtual sf::Rect<float> getBound(){
			return bound_rect;
		}
		virtual void update( sf::RenderWindow *window ){
			// all event handles are done in parent screen.
			// the method here only handles passive
			// updates, such as sprite animation.
		}
		virtual void draw( sf::RenderWindow *window ){
			// every sprite should only just draw from window.
			// the parent screen handles drawing everything in
			// one loop.
		}
		Sprite(){
			// default position of sprite is at 0,0 of screen
			bound_rect.left = 0;
			bound_rect.top = 0;
			// default origin is on 0,0 of spritesheet
			draw_rect.left = 0;
			draw_rect.top = 0;
			// default no movement
			move_vector.x = 0;
			move_vector.y = 0;
			// default movement is north
			move_angle = 0;
		}
		~Sprite(){
		}
	};

	class Sprite_Ship : public Sprite{
	private:
		enum ShipState{
			SHIP_INIT = 0,
			SHIP_SPAWN,
			SHIP_ALIVE,
			SHIP_EXPLODE,
			SHIP_DEAD
		};
		int life;
		enum ShipMove{
			MOVE_FORWARD,
			MOVE_BACKWARD,
			MOVE_FREE
		} movement;
		enum ShipTurn{
			TURN_LEFT,
			TURN_RIGHT,
			TURN_FREE
		} turning;
		float accel_speed;
		float decel_speed;
		float turn_speed;
		float max_speed;
		ShipState ship_state;
	public:
		void setState( ShipState s ){
			ship_state = s;
			switch (ship_state){
				case SHIP_INIT:
				case SHIP_ALIVE:
					draw_rect.left = 32;
					draw_rect.top = 16;
					break;

				case SHIP_SPAWN:
					break;

				case SHIP_EXPLODE:
					break;

				case SHIP_DEAD:
					break;
			}
		}
		void setSpritesheet( sf::Texture *ss ){
			// set the sprite, then clip the sprite.
			// make sure sprite is clipped instead of
			// showing the whole spritesheet.
			Sprite::setSpritesheet( ss );
			sprite.setTextureRect( draw_rect );
		}
		sf::Vector2f transformPoint( sf::Vector2f p1, sf::Vector2f p2, float angle ){
			float rads = float(angle / 180.f * ivy::PI);
			float x = p1.x + (p2.x - p1.x) * std::cos(-rads) + (p2.y - p1.y) * std::sin(-rads);
			float y = p1.y - (p2.x - p1.x) * std::sin(-rads) + (p2.y - p1.y) * std::cos(-rads);
			return sf::Vector2f( x, y );
		}
		sf::Vector2f getVector( float magnitude, float angle ){
			float rads = float(angle / 180.f * ivy::PI);
			sf::Vector2f point = sf::Vector2f( 0, -magnitude );
			float x = point.x * std::cos(-rads) + point.y * std::sin(-rads);
			float y = point.x * std::sin(-rads) + point.y * std::cos(-rads);
			return sf::Vector2f( x, y );
		}

		// life

		void addLife(){
			--life;
		}
		void minusLife(){
			++life;
			if (life <= 0){
				setState( SHIP_DEAD );
			}
		}

		// positive movement

		void accelerate(){
			// keep adding velocity until stopAccelerate is called
			movement = MOVE_FORWARD;
		}
		void decelerate(){
			// keep reducing velocity until stopDecelerate is called
			movement = MOVE_BACKWARD;
		}
		void turnLeft(){
			// keep turning until stopTurn is called
			turning = TURN_LEFT;
		}
		void turnRight(){
			// keep turning until stopTurn is called
			turning = TURN_RIGHT;
		}

		// stop movement

		void stopAccelerate(){
			movement = MOVE_FREE;
		}
		void stopDecelerate(){
			movement = MOVE_FREE;
		}
		void stopTurn(){
			turning = TURN_FREE;
		}

		// update / draw / constructor

		void update( sf::Int32 time_elapsed ){

			float delta = time_elapsed / 1000.f;

			// rotate the ship
			float old_angle = sprite.getRotation();
			float new_angle = 0.f;
			switch ( turning ){
				case TURN_LEFT:
					new_angle = -(delta * turn_speed);
					break;
				case TURN_RIGHT:
					new_angle = (delta * turn_speed);
					break;
				case TURN_FREE:
				default:
					break;
			}
			float final_angle = old_angle + new_angle;
			sprite.setRotation( final_angle );

			// update the ship movement
			// float old_speed = delta * speed;
			float new_speed = 0.f;
			bool moved = false;
			switch ( movement ){
				case MOVE_FORWARD:
					new_speed = (delta * accel_speed);
					moved = true;
					break;
				case MOVE_BACKWARD:
					new_speed = -(delta * decel_speed);
					moved = true;
					break;
				case MOVE_FREE:
				default:
					break;
			}

			// calculate the movement vector
			if (moved){
				// if the ship moved in a different direction,
				// add the new movement vector to current movement
				sf::Vector2f new_vector = getVector( new_speed, final_angle );
				move_vector += new_vector;
				std::cout << "pos :" << new_vector.x << "," << new_vector.y << " / " << move_vector.x << "," << move_vector.y << "\n";
			}
			float cx = move_vector.x;
			float cy = move_vector.y;

			// cap the vector's magnitude
			float curr_speed = std::sqrt((cx*cx)+(cy*cy));
			if (curr_speed > max_speed){
				move_vector.x *= (max_speed / curr_speed);
				move_vector.y *= (max_speed / curr_speed);
			}
			float new_x = (delta * move_vector.x);
			float new_y = (delta * move_vector.y);
			bound_rect.left += new_x;
			bound_rect.top += new_y;

			// update ship position
			// this formula moves the ship like a CAR, not a ship moving in free space
			// float vector = delta * speed;
			// float rads = float(final_angle / 180.f * ivy::PI);
			// sf::Vector2f center = sf::Vector2f( bound_rect.left, bound_rect.top );
			// sf::Vector2f point = sf::Vector2f( bound_rect.left, bound_rect.top - vector );
			// float x = center.x + (point.x - center.x) * std::cos(-rads) + (point.y - center.y) * std::sin(-rads);
			// float y = center.y - (point.x - center.x) * std::sin(-rads) + (point.y - center.y) * std::cos(-rads);
			// bound_rect.left = x;
			// bound_rect.top = y;

			// if the ship moved out of screen bounds, wrap it
			// NOTE :
			// the ship's top/left are already centered because we
			// setOrigin() it to the center, but not the globalBounds.
			sf::Rect<float> bounds = sprite.getGlobalBounds();
			float center_x = bounds.left + (bounds.width/2.f);
			float center_y = bounds.top + (bounds.height/2.f);
			if ( center_y < 0 ){
				bound_rect.top = ivy::WINDOW_H + new_y;
			}
			if ( center_y > ivy::WINDOW_H ){
				bound_rect.top = + new_y;
			}
			if ( center_x < 0 ){
				bound_rect.left = ivy::WINDOW_W + new_x;
			}
			if ( center_x > ivy::WINDOW_W ){
				bound_rect.left = new_x;
			}

			// finalize the ship's position
			sprite.setPosition( bound_rect.left, bound_rect.top );
		}
		void draw( sf::RenderWindow *window ){
			// draw the ship
			(*window).draw( sprite );
		}
		Sprite_Ship(){

			// default ship stats
			movement = MOVE_FREE;
			turning = TURN_FREE;
			accel_speed = 800.f; // add velocity by 800px/s
			decel_speed = 800.f;
			turn_speed = 180.f;
			max_speed = 400.f; // maximum velocity

			// thus, we reach max speed in 0.5 seconds (400 / 800)

			// ship hitbox
			bound_rect.width = 4;
			bound_rect.height = 4;

			// ship drawable size
			draw_rect.width = 8;
			draw_rect.height = 16;

			// set the center point of the ship based on its width/height
			float origin_x = draw_rect.width / 2.f;
			float origin_y = draw_rect.height / 2.f;
			sprite.setOrigin( origin_x, origin_y );

			// default starting life = 3;
			life = 3;

			// update state
			setState( SHIP_INIT );
		}
		~Sprite_Ship(){
		}
	};

	class Sprite_Bullet : public Sprite{
	private:
	public:
	};

	class Sprite_Rock : public Sprite{
	private:
	public:
	};
}

#endif