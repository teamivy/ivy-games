#ifndef IVY_ENUMS_H
#define IVY_ENUMS_H

namespace ivy{
	enum GameState{
		GAME_NOTHING = 0, // do nothing
		GAME_INTRO,
		GAME_MAIN_MENU,
		GAME_PLAY,
		GAME_OVER,
		GAME_EXIT
	};

	enum ScreenState{
		SCREEN_INIT = 0,
		SCREEN_TRANSITION_IN,
		SCREEN_ACTIVE,
		SCREEN_INACTIVE,
		SCREEN_TRANSITION_OUT,
		SCREEN_DEAD
	};
}

#endif