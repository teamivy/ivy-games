#include <SFML/Graphics.hpp>

#include <iostream>
using namespace std;

enum GameState{
	Quit = 0,
	Menu,
	Game,
	Score
};

GameState state;

sf::Font font;
int menu_selected_index;
int menu_selected_alpha;
int menu_selected_alpha_dir;
char* _menu_items[2];

int game_player_turn; // 1 = player, 2 = AI
sf::Vector2<int> game_position;
int game_board[3][3];
int game_icon_size;
int game_icon_padding;
int game_highlight_alpha;
int game_highlight_alpha_dir;
sf::Texture game_spritesheet;

int score_win;
int score_lose;
int score_draw;
int score_winner;
sf::Font score_font;
int score_selected_index;
int score_selected_alpha;
int score_selected_alpha_dir;
char* _score_items[2];

void reset_board(){
	// reset player variables
	game_position.x = 1;
	game_position.y = 1;
	game_player_turn = 1;

	// reset the flags
	for (int i = 3; i--;){
		for (int j = 3; j--;){
			game_board[i][j] = 0;
		}
	}
}

void init_menu(){
	_menu_items[0] = "Start Game";
	_menu_items[1] = "Quit Game";
	menu_selected_index = 0;
	menu_selected_alpha = 255;
	menu_selected_alpha_dir = -5;

	// load font
    if ( !font.loadFromFile("assets/georgia.ttf") )
        throw 0;
}
void update_menu( sf::RenderWindow *window ){
	// create event for polling
	sf::Event event;
	bool menu_changed = false;
    while ( (*window).pollEvent(event))
    {
		// get key code
		sf::Keyboard::Key key = event.key.code;

		// close window
        if (event.type == sf::Event::Closed)
            (*window).close();

		// move highlight up/down
		if (event.type == sf::Event::KeyPressed){
			if (key == sf::Keyboard::Up){
				menu_changed = true;
				menu_selected_index--;
			}
			if (key == sf::Keyboard::Down){
				menu_changed = true;
				menu_selected_index++;
			}
			if (key == sf::Keyboard::Return){
				// reset variables and switch state
				menu_changed = true;
				if (menu_selected_index == 0){
					reset_board();
					state = Game;
				}
				else{
					state = Quit;
				}
			}
		}
    }

	// post-event processing
	if (menu_selected_index < 0){
		menu_selected_index = 0;
	}
	if (menu_selected_index > 1){
		menu_selected_index = 1;
	}

	if (menu_changed){
		menu_selected_alpha = 255;
		menu_selected_alpha_dir = -5;
	}

	menu_selected_alpha += menu_selected_alpha_dir;
	if (menu_selected_alpha < 50){
		menu_selected_alpha = 50;
		menu_selected_alpha_dir *= -1;
	}
	if (menu_selected_alpha > 255){
		menu_selected_alpha = 255;
		menu_selected_alpha_dir *= -1;
	}
}
void draw_menu( sf::RenderWindow *window ){
	for (int i = 0; i < 2; i++){
		// set variables
		const char* t = _menu_items[i];
		sf::Text text(t, font, 50);
		float x = 50;
		float y = float(i * 60 + 50);
		sf::Color color;

		// if selected, is flashing red.
		// else, white
		if (i == menu_selected_index){
			color = sf::Color( 255, 0, 0, menu_selected_alpha );
		}
		else{
			color = sf::Color::White;
		}

		// set text properties
		text.setPosition( x, y );
		text.setColor( color );

		// draw text
		(*window).draw( text );
	}
}

void init_game(){
	game_player_turn = 1;
	game_position.x = 1;
	game_position.y = 1;
	game_icon_size = 100;
	game_icon_padding = 20;
	game_highlight_alpha = 255;
	game_highlight_alpha_dir = -3;

	reset_board();

    if ( !game_spritesheet.loadFromFile("assets/spritesheet.png") )
        throw 0;
}
void update_game( sf::RenderWindow *window ){

	bool player_chose = false;

	// cout << "PLAYER TURN : " << game_player_turn << "\n";

	if ( game_player_turn == 1 ){
		// player's turn
		sf::Event event;
		while ( (*window).pollEvent(event))
		{
			// get key code
			sf::Keyboard::Key key = event.key.code;

			// close window
			if (event.type == sf::Event::Closed)
				(*window).close();

			// move tic tac toe cursor based on mouse position
			if (event.type == sf::Event::MouseMoved){
				int min = 50;
				int max = 50 + (2 * (game_icon_size + game_icon_padding)) + game_icon_size;
				int x = event.mouseMove.x;
				int y = event.mouseMove.y;
				if (x > min &&
					x < max &&
					y > min &&
					y < max){
						cout << "mouse within playable area\n";
						int offset = game_icon_size + game_icon_padding;
						if (x > 50 && x < (50 + game_icon_size)){
							game_position.x = 0;
						}
						if (x > (50 + offset) && x < (50 + offset + game_icon_size)){
							game_position.x = 1;
						}
						if (x > (50 + offset*2) && x < (50 + (offset + game_icon_size)*2)){
							game_position.x = 2;
						}
						if (y > 50 && y < (50 + game_icon_size)){
							game_position.y = 0;
						}
						if (y > (50 + offset) && y < (50 + offset + game_icon_size)){
							game_position.y = 1;
						}
						if (y > (50 + offset*2) && y < (50 + (offset + game_icon_size)*2)){
							game_position.y = 2;
						}
				}
			}

			if (event.type == sf::Event::MouseButtonPressed){
				if (event.mouseButton.button == sf::Mouse::Left){
					if (game_board[ game_position.y ][ game_position.x ] == 0){
						player_chose = true;
					}
				}
			}

			// move tic tac toe cursor
			if (event.type == sf::Event::KeyPressed){
				if (key == sf::Keyboard::Up){
					game_position.y -= 1;
				}
				if (key == sf::Keyboard::Down){
					game_position.y += 1;
				}
				if (key == sf::Keyboard::Left){
					game_position.x -= 1;
				}
				if (key == sf::Keyboard::Right){
					game_position.x += 1;
				}

				if (key == sf::Keyboard::Return){
					if (game_board[ game_position.y ][ game_position.x ] == 0){
						player_chose = true;
					}
				}
			}
		}

		// loop the cursor around the tic tac toe board
		if (game_position.x < 0){
			game_position.x = 2;
		}
		if (game_position.x > 2){
			game_position.x = 0;
		}
		if (game_position.y < 0){
			game_position.y = 2;
		}
		if (game_position.y > 2){
			game_position.y = 0;
		}

		// cout << "position at :" << game_position.x << "," << game_position.y << "\n";
	}
	else{
		// AI's turn

		// for now, just pick something that's empty
		for (int i = 3; i--;){
			for (int j = 3; j--;){
				if ( game_board[i][j] == 0 ){
					game_position.x = j;
					game_position.y = i;
					player_chose = true;
				}
			}
		}
	}

	// update highlight alpha
	game_highlight_alpha += game_highlight_alpha_dir;
	if (game_highlight_alpha < 50){
		game_highlight_alpha = 50;
		game_highlight_alpha_dir *= -1;
	}
	if (game_highlight_alpha > 150){
		game_highlight_alpha = 150;
		game_highlight_alpha_dir *= -1;
	}

	// if player pressed enter, do the set X/O and switch turns
	if (player_chose){
		cout << "PLAYER #" << game_player_turn << " chosen.\n";
		// set the current slot as "chosen" by current player
		game_board[ game_position.y ][ game_position.x ] = game_player_turn;

		// switch the player's turn
		if (game_player_turn == 1){
			game_player_turn = 2;
		}
		else{
			game_player_turn = 1;
		}
	}

	// check game rules

	// is there a win?
	int win = 0;
	for (int p = 1; p < 3; p++){
		// check win for each player
		if (game_board[0][0] == p &&
			game_board[0][1] == p &&
			game_board[0][2] == p){
				win = p;
		}
		if (game_board[1][0] == p &&
			game_board[1][1] == p &&
			game_board[1][2] == p){
				win = p;
		}
		if (game_board[2][0] == p &&
			game_board[2][1] == p &&
			game_board[2][2] == p){
				win = p;
		}

		if (game_board[0][0] == p &&
			game_board[1][0] == p &&
			game_board[2][0] == p){
				win = p;
		}
		if (game_board[0][1] == p &&
			game_board[1][1] == p &&
			game_board[2][1] == p){
				win = p;
		}
		if (game_board[0][2] == p &&
			game_board[1][2] == p &&
			game_board[2][2] == p){
				win = p;
		}

		if (game_board[0][0] == p &&
			game_board[1][1] == p &&
			game_board[2][2] == p){
				win = p;
		}
		if (game_board[0][2] == p &&
			game_board[1][1] == p &&
			game_board[2][0] == p){
				win = p;
		}
	}

	if (win){
		cout << "PLAYER #" << win << " WON\n";
		score_winner = win;
		if (win == 1){
			score_win++;
		}
		else{
			score_lose++;
		}

		// move to score screen, make sure highlight is always at "Replay"
		score_selected_index = 0;
		state = Score;
	}
	else{
		// is there a stalemate?
		bool stalemate = true;
		for (int i = 3; i--;){
			for (int j = 3; j--;){
				if ( game_board[i][j] == 0 ){
					stalemate = false;
					break;
				}
			}
		}
		if (stalemate){
			cout << "No player won...\n";
			score_draw++;
			score_winner = 0;
			state = Score;
		}
	}
}
void draw_game( sf::RenderWindow *window ){
	for (int i = 3; i--;){
		for (int j = 3; j--;){
			// set texture origin for icon
			sf::Sprite sprite( game_spritesheet );
			bool has_sprite = false;
			if ( game_board[i][j] == 1 ){
				has_sprite = true;
				sprite.setTextureRect( sf::IntRect( 0, 0, 50, 50 ) );
			}
			else if ( game_board[i][j] == 2 ){
				has_sprite = true;
				sprite.setTextureRect( sf::IntRect( 50, 0, 50, 50 ) );
			}

			// set position of slot/icon
			int x = 50 + (j * (game_icon_size + game_icon_padding));
			int y = 50 + (i * (game_icon_size + game_icon_padding));

			// draw highlightable box so we know where the board is
			sf::RectangleShape rect;
			rect.setFillColor( sf::Color( 0, 255, 0, 50 ) );
			rect.setPosition( x, y );
			rect.setSize( sf::Vector2f( game_icon_size, game_icon_size) );
			(*window).draw( rect );

			// draw the X/O icons
			if (has_sprite){
				sprite.setPosition( x, y );
				sprite.setScale( 2, 2 );
				(*window).draw( sprite );
			}
		}
	}

	// draw player's highlight
	sf::RectangleShape highlight;
	int hl_x = 50 + ( game_position.x * (game_icon_size + game_icon_padding) );
	int hl_y = 50 + ( game_position.y * (game_icon_size + game_icon_padding) );
	highlight.setFillColor( sf::Color( 0, 0, 255, game_highlight_alpha ) );
	highlight.setPosition( hl_x, hl_y );
	highlight.setSize( sf::Vector2f( game_icon_size, game_icon_size ) );
	(*window).draw( highlight );
}

void init_score(){
	score_win = 0;
	score_lose = 0;
	score_draw = 0;
	score_winner = 0;
	_score_items[0] = "Replay";
	_score_items[1] = "Back to Menu";
	score_selected_index = 0;
	score_selected_alpha = 255;
	score_selected_alpha_dir = -5;
}
void update_score( sf::RenderWindow *window ){
	// create event for polling
	sf::Event event;
	bool menu_changed = false;
    while ( (*window).pollEvent(event))
    {
		// get key code
		sf::Keyboard::Key key = event.key.code;

		// close window
        if (event.type == sf::Event::Closed)
            (*window).close();

		// move highlight up/down
		if (event.type == sf::Event::KeyPressed){
			if (key == sf::Keyboard::Up){
				menu_changed = true;
				score_selected_index--;
			}
			if (key == sf::Keyboard::Down){
				menu_changed = true;
				score_selected_index++;
			}
			if (key == sf::Keyboard::Return){
				// reset variables and switch state
				menu_changed = true;
				if (score_selected_index == 0){
					reset_board();
					state = Game;
				}
				else{
					state = Menu;
				}
			}
		}
    }

	// post-event processing
	if (score_selected_index < 0){
		score_selected_index = 0;
	}
	if (score_selected_index > 1){
		score_selected_index = 1;
	}

	if (menu_changed){
		score_selected_alpha = 255;
		score_selected_alpha_dir = -5;
	}

	score_selected_alpha += score_selected_alpha_dir;
	if (score_selected_alpha < 50){
		score_selected_alpha = 50;
		score_selected_alpha_dir *= -1;
	}
	if (score_selected_alpha > 255){
		score_selected_alpha = 255;
		score_selected_alpha_dir *= -1;
	}
}
void draw_score( sf::RenderWindow *window ){
	for (int i = 0; i < 2; i++){
		// set variables
		const char* t = _score_items[i];
		sf::Text text(t, font, 50);
		float x = 50;
		float y = float(i * 60 + 50);
		sf::Color color;

		// if selected, is flashing red.
		// else, white
		if (i == score_selected_index){
			color = sf::Color( 255, 0, 0, score_selected_alpha );
		}
		else{
			color = sf::Color::White;
		}

		// set text properties
		text.setPosition( x, y );
		text.setColor( color );

		// draw text
		(*window).draw( text );
	}

	// set the color, text based on win/lose/draw
	const char* msg;
	sf::Color msg_color;
	if (score_winner){
		if (score_winner == 1){
			msg = "You won!";
			msg_color.g = 255;
		}
		else{
			msg = "You lost...";
			msg_color.r = 255;
		}
	}
	else{
		msg = "It's a draw!";
		msg_color.b = 255;
	}
	sf::Text msg_text( msg, font, 35 );
	msg_text.setColor( msg_color );
	msg_text.setPosition( 100, 250 );
	(*window).draw( msg_text );

	// show statistics
	// to-do - concatenate strings into one line before displaying...
}

int main(){
	// Create the main window
    sf::RenderWindow window( sf::VideoMode(800, 600), "Team Tac Toe" );
	window.setFramerateLimit(60);
	window.setKeyRepeatEnabled(false);

	// default start from menu
	state = Menu;
	
	// initialize game variables
	init_menu();
	init_game();
	init_score();

    // Start the game loop
    while (window.isOpen())
    {
		// update game
		switch( state ){
		case Menu:
			update_menu( &window );
			break;
		case Game:
			update_game( &window );
			break;
		case Score:
			update_score( &window );
			break;
		case Quit:
			window.close();
			break;
		default:
			break;
		}

        // clear screen
        window.clear();

		// draw to screen
		switch( state ){
		case Menu:
			draw_menu( &window );
			break;
		case Game:
			draw_game( &window );
			break;
		case Score:
			draw_score( &window );
			break;
		default:
			break;
		}

        // render screen
        window.display();
    }

    return EXIT_SUCCESS;
}