#include "ivy_globals.h"
#include "ivy_enums.h"
#include "ivy_screens.h"

int main(){
	// create window and set parameters
	sf::RenderWindow window( sf::VideoMode( ivy::WINDOW_W, ivy::WINDOW_H ), ivy::GAME_TITLE );
	window.setFramerateLimit(60);
	window.setKeyRepeatEnabled(false);

	// init screen manager
	ivy::ScreenManager manager;
	manager.setWindow( &window );

	// add first screen
	manager.addScreen( ivy::GAME_INTRO );

	// game loop
	while ( window.isOpen() )
	{
		// update all screen states
		manager.update();

		// draw all screens
		manager.draw();
	}

	return 0;
}