#ifndef IVY_SCREENS_H
#define IVY_SCREENS_H

#include <iostream>
#include <sstream>
#include <vector>
using namespace std;

#include "ivy_enums.h"

namespace ivy{

	// ==================================================
	// 
	// game screens
	// 
	// ==================================================

	class Screen{
	protected:
		static int ID_COUNTER;
		int screen_id;
		sf::RenderWindow *window;
		ivy::ScreenState screen_state;
		ivy::GameState next_screen;
	public:
		Screen(){
			screen_id = ++ID_COUNTER;
			screen_state = ivy::SCREEN_INIT;
			next_screen = ivy::GAME_NOTHING;
			cout << "created screen :" << screen_id << "\n";
		}
		~Screen(){
			cout << "destroying screen :" << screen_id << "\n";
			// delete window;
		}
		int getScreenId(){
			return screen_id;
		}
		ivy::ScreenState getScreenState(){
			return screen_state;
		}
		ivy::GameState getNextScreen(){
			return next_screen;
		}

		void setWindow( sf::RenderWindow *w ){
			window = w;
		}

		void update(){
			switch (screen_state){
				case ivy::SCREEN_INIT:
					// load resources
					updateInit();
					break;

				case ivy::SCREEN_TRANSITION_IN:
					// transition the curtain alpha
					updateTransitionIn();
					break;

				case ivy::SCREEN_ACTIVE:
					// main logic goes here
					updateActive();
					break;

				case ivy::SCREEN_INACTIVE:
					// don't do anything, but can still draw
					updateInactive();
					break;

				case ivy::SCREEN_TRANSITION_OUT:
					// transition the curtain alpha
					updateTransitionOut();
					break;
			}
		}
		virtual void updateInit(){
			cout << "===== updateInit\n";
			screen_state = ivy::SCREEN_TRANSITION_IN;
		}
		virtual void updateTransitionIn(){
			cout << "===== updateTransitionIn\n";
			screen_state = ivy::SCREEN_ACTIVE;
		}
		virtual void updateActive(){
			// cout << "===== updateActive\n";
			// if not overridden, at least listen for close
			sf::Event e;
			while ( (*window).pollEvent(e) ){
				if ( e.type == sf::Event::Closed ){
					(*window).close();
				}
			}
			// screen_state = ivy::SCREEN_INACTIVE;
		}
		virtual void updateInactive(){
			cout << "===== updateInactive\n";
			// if not overridden, at least listen for close
			sf::Event e;
			while ( (*window).pollEvent(e) ){
				if ( e.type == sf::Event::Closed ){
					(*window).close();
				}
			}
			screen_state = ivy::SCREEN_TRANSITION_OUT;
		}
		virtual void updateTransitionOut(){
			cout << "===== updateTransitionOut\n";
			screen_state = ivy::SCREEN_DEAD;
		}

		void draw(){
			switch (screen_state){
				case ivy::SCREEN_INIT:
					drawInit();
					break;

				case ivy::SCREEN_TRANSITION_IN:
					drawTransitionIn();
					break;

				case ivy::SCREEN_ACTIVE:
					drawActive();
					break;

				case ivy::SCREEN_INACTIVE:
					drawInactive();
					break;

				case ivy::SCREEN_TRANSITION_OUT:
					drawTransitionOut();
					break;
			}
		}
		virtual void drawInit(){}
		virtual void drawTransitionIn(){}
		virtual void drawActive(){}
		virtual void drawInactive(){}
		virtual void drawTransitionOut(){}
	};

	class Screen_Intro : public Screen{
	public:
		Screen_Intro(){}
		~Screen_Intro(){}

		void drawActive(){
			next_screen = ivy::GAME_MAIN_MENU;
			screen_state = ivy::SCREEN_TRANSITION_OUT;
		}
	};

	class Screen_Main_Menu : public Screen{
	public:
		Screen_Main_Menu(){}
		~Screen_Main_Menu(){}

		void drawActive(){
			// to-do
			// check for menu input UP/DOWN/ENTER
			// update menu highlight
		}
	};

	class Screen_Play : public Screen{
	private:
		static int HI_SCORE;
		int score;
	public:
		Screen_Play(){
			score = 0;
		}
		~Screen_Play(){}

		void drawActive(){
			// to-do
			// check for WSAD for movement
			// check for spacebar for shoot
			// update player
			// update bullets
			// update asteroids
			// check collision between bullet/asteroid
			// check collision between player/asteroid
			// update score
			// if died, start timer to respawn after 3 seconds
			// if no more life, go to game over screen
		}

		int getGameScore(){
			// to be used by game over screen to show total score
			return score;
		}
	};

	class Screen_Over : public Screen{
	public:
		Screen_Over(){}
		~Screen_Over(){}

		void drawActive(){
			// to-do
			// check for menu input UP/DOWN/ENTER
			// update menu highlight
			// show score and high score
		}
	};

	// ==================================================
	// 
	// screen manager
	// 
	// ==================================================

	class ScreenManager{
	private:
		std::vector<Screen*> _screens;
		sf::RenderWindow *window;

		ivy::Screen* getScreenById( int target_id = 0 ){
			std::vector<Screen*>::iterator i;
			ivy::Screen* screen;
			for (i = _screens.begin(); i != _screens.end(); ++i){
				int id = (*i)->getScreenId();
				if (id == target_id){
					screen = *(i._Ptr);
				}
			}
			return screen;
		}
		void removeScreenById( int id ){
			// the actual screen removal is a manager-only method.
			// when deleting a screen, we actually transition it out
			// before setting SCREEN_DEAD state to it, which will
			// trigger this function from update();
			std::vector<Screen*>::iterator it;
			for (it = _screens.begin(); it != _screens.end(); it++){
				int this_id = (*it)->getScreenId();
				if ( this_id == id ){
					cout << "remove screen : " << id << "\n";
					// http://www.cplusplus.com/reference/vector/vector/erase/
					_screens.erase( it );
					break;
				}
			}
		}
	public:
		void update(){
			// iterate from last to first, because the latest
			// screen is usually the last in the list
			std::vector<int> _to_delete;
			ivy::GameState next_screen = ivy::GAME_NOTHING;

			// if the list is empty, quit!
			if ( _screens.begin() == _screens.end() ){
				(*window).close();
				return;
			}

			std::vector<Screen*>::reverse_iterator rit;
			for (rit = _screens.rbegin(); rit != _screens.rend(); ++rit){

				// update screen
				(*rit)->update();

				// get current state
				ivy::ScreenState state = (*rit)->getScreenState();

				// if screen is dead:
				if ( state == ivy::SCREEN_DEAD ){
					// add id to _to_delete
					int this_id = (*rit)->getScreenId();
					_to_delete.push_back( this_id );

					// get next screen to show (if any)
					next_screen = (*rit)->getNextScreen();
				}
			}

			// iterate through list and delete screens in _to_delete
			std::vector<int>::iterator it;
			for (it = _to_delete.begin(); it != _to_delete.end(); ++it){
				removeScreenById( *it );
			}

			// if there is a next screen, add it to queue
			if ( next_screen != ivy::GAME_NOTHING ){
				addScreen( next_screen );
			}
		}
		void draw(){
			std::vector<Screen*>::reverse_iterator i;
			for (i = _screens.rbegin(); i != _screens.rend(); ++i){
				// draw screen
				(*i)->draw();
			}
		}
		void addScreen( ivy::GameState game_state ){
			cout << "addScreen : ";
			Screen* screen;
			switch ( game_state ){
				case ivy::GAME_INTRO:
					cout << "GAME_INTRO\n";
					screen = new Screen_Intro();
					break;

				case ivy::GAME_MAIN_MENU:
					cout << "GAME_MAIN_MENU\n";
					screen = new Screen_Main_Menu();
					break;

				case ivy::GAME_PLAY:
					cout << "GAME_PLAY\n";
					screen = new Screen_Play();
					break;

				case ivy::GAME_OVER:
					cout << "GAME_OVER\n";
					screen = new Screen_Over();
					break;

				case ivy::GAME_NOTHING:
				default:
					// do nothing
					return;
					break;
			}

			// NOTE
			// will throw error if screen is not init
			// so be careful!
			(*screen).setWindow( window );
			_screens.push_back( screen );
		}
		void setWindow( sf::RenderWindow *w ){
			window = w;
		}
	};

	// init static variables
	int Screen_Play::HI_SCORE = 0;
	int Screen::ID_COUNTER = 0;
}

#endif