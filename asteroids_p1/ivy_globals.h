#ifndef IVY_GLOBALS_H
#define IVY_GLOBALS_H

#include <sstream>
#include <iostream>

namespace ivy{
	const char* GAME_TITLE = "Team Asteroids";
	const unsigned int WINDOW_W = 800;
	const unsigned int WINDOW_H = 600;
}

#endif