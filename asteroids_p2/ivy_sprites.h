#ifndef IVY_SPRITES_H
#define IVY_SPRITES_H

#include <vector>
using namespace std;

#include <SFML\Graphics.hpp>
#include "ivy_enums.h"

namespace ivy{

	// to-do
	// load sprite resources in threads
	// use flags to signal that a sprite is done loading resources
	// make sure screens check the sprite flags for done loading before changing state from SCREEN_INIT to SCREEN_TRANSITION_IN
	
	// ==================================================
	// 
	// sprites
	// 
	// ==================================================

	class Sprite{
	protected:
		static int ID_COUNTER;
		int sprite_id;
		sf::Sprite sprite; // current sprite being drawn at x/y position with w/h size

		sf::Rect<int> bound_rect; // actual position/bounds of this sprite
		sf::Rect<int> draw_rect; // drawable area inside spritesheet
	public:
		virtual void setSpritesheet( sf::Texture *ss ){
			sprite.setTexture( (*ss) );
		}
		virtual void setPosition( int x, int y ){
			// update the x/y position of sprite
			bound_rect.left = x;
			bound_rect.top = y;
		}
		virtual sf::Rect<int> getBound(){
			return bound_rect;
		}
		virtual void update( sf::RenderWindow *window ){
			// all event handles are done in parent screen.
			// the method here only handles passive
			// updates, such as sprite animation.
		}
		virtual void draw( sf::RenderWindow *window ){
			// every sprite should only just draw from window.
			// the parent screen handles drawing everything in
			// one loop.
		}
		Sprite(){
			// default position of sprite is at 0,0 of screen
			bound_rect.left = 0;
			bound_rect.top = 0;
			// default origin is on 0,0 of spritesheet
			draw_rect.left = 0;
			draw_rect.top = 0;
		}
		~Sprite(){
		}
	};

	class Sprite_Ship : public Sprite{
	private:
		enum ShipState{
			SHIP_INIT = 0,
			SHIP_SPAWN,
			SHIP_ALIVE,
			SHIP_EXPLODE,
			SHIP_DEAD
		};
		int life;
		ShipState ship_state;
	public:
		void setState( ShipState s ){
			ship_state = s;
			switch (ship_state){
				case SHIP_INIT:
				case SHIP_ALIVE:
					draw_rect.left = 32;
					draw_rect.top = 16;
					break;

				case SHIP_SPAWN:
					break;

				case SHIP_EXPLODE:
					break;

				case SHIP_DEAD:
					break;
			}
		}
		void setSpritesheet( sf::Texture *ss ){
			// set the sprite, then clip the sprite.
			// make sure sprite is clipped instead of
			// showing the whole spritesheet.
			Sprite::setSpritesheet( ss );
			sprite.setTextureRect( draw_rect );
		}
		void addLife(){
			--life;
		}
		void minusLife(){
			++life;
			if (life <= 0){
				setState( SHIP_DEAD );
			}
		}
		void update( sf::RenderWindow *window ){
			// update the ship position
			sprite.setPosition( bound_rect.left, bound_rect.top );
		}
		void draw( sf::RenderWindow *window ){
			// draw the ship
			(*window).draw( sprite );
		}
		Sprite_Ship(){
			// ship hitbox
			bound_rect.width = 4;
			bound_rect.height = 4;

			// ship drawable size
			draw_rect.width = 8;
			draw_rect.height = 16;

			// default starting life = 3;
			life = 3;

			// update state
			setState( SHIP_INIT );
		}
		~Sprite_Ship(){
		}
	};

	class Sprite_Bullet : public Sprite{
	private:
	public:
	};

	class Sprite_Rock : public Sprite{
	private:
	public:
	};
}

#endif