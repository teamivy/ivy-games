#include <SFML\Graphics.hpp>

#include "ivy_globals.h"
#include "ivy_enums.h"
#include "ivy_screens.h"

int main(){
	// create window and set parameters
	sf::RenderWindow window( sf::VideoMode( ivy::WINDOW_W, ivy::WINDOW_H ), ivy::GAME_TITLE );
	window.setFramerateLimit(30);
	window.setKeyRepeatEnabled(false);

	// init screen manager
	ivy::ScreenManager manager;
	manager.setWindow( &window );

	// add first screen
	manager.addScreen( ivy::GAME_INTRO );

	// game loop
	while ( window.isOpen() )
	{
		// update all screen states
		manager.update();

		// clear the screen
		window.clear();

		// draw all screens
		manager.draw();

		// render the screen
		window.display();
	}

	return 0;
}