#ifndef IVY_GLOBALS_H
#define IVY_GLOBALS_H

namespace ivy{
	// game constants
	const char* GAME_TITLE = "Team Asteroids";
	const unsigned int WINDOW_W = 800;
	const unsigned int WINDOW_H = 600;

	// game variables
	static int HI_SCORE = 0;
	static int SCORE = 0;
}

#endif